#!/usr/bin/env bash

set -euo pipefail

authenticationServer=https://devgeodesy-openam.geodesy.ga.gov.au/openam/oauth2

mvn clean install spring-boot:run \
    -DjwksUri=$authenticationServer/connect/jwk_uri \
    -Doauth2Uri=$authenticationServer \
    -DclientId= \
    -DclientSecret=
