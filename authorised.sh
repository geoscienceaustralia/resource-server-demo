#!/usr/bin/env bash

set -euo pipefail

idToken=$1

curl -i http://localhost:8080/api/authorised \
    -H "Authorization: Bearer $idToken"
