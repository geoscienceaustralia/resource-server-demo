#!/usr/bin/env bash

set -euo pipefail

# "id_token" or "access_token"
tokenName=$1

TOKEN_URI=https://devgeodesy-openam.geodesy.ga.gov.au/openam/oauth2/access_token

CLIENT_ID=
CLIENT_SECRET=
USERNAME=
PASSWORD=

tokenResponse=$(curl -s -XPOST "$TOKEN_URI" \
    -u "$CLIENT_ID:$CLIENT_SECRET" \
    -d "grant_type=password" \
    -d "username=$USERNAME" \
    -d "password=$PASSWORD" \
    -d "scope=openid profile")

jq -r .$tokenName <<< "$tokenResponse"
