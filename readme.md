# GA GNSS Data - OAuth2 Resource Server Demo

## About

This repository uses the Spring Framework for Java to stand up an OAuth2
resource server.  It demonstrates how to configure Spring Security to accept
both access tokens and id tokens issued by an Oauth2 server, in this case
OpenAM. Both stateless and stateful (opaque) access tokens are supported,
however, the authentication server used in this example packs neither the KID
nor the user's groups into stateless access tokens. The resource server must
therefore make two trips per request to the authentication server, one to
validate the token (/introspect) and another to fetch user groups (/userinfo),
while to accept an id token, the resource server need not contact the
authentication server at all.

## Requirements

jdk8, maven

You can optionally install the required software using [nix](https://nixos.org/nix/),
otherwise skip to step 3.

1) Install nix

```bash
$ curl https://nixos.org/nix/install | sh
```

2) Enter [nix shell](https://nixos.wiki/wiki/Development_environment_with_nix-shell)
to install the requirements as specified in `./shell.nix`.

```bash
$ nix-shell
```

## Running the resource server

1) Build and run

```bash
$ ./run.sh
```

(Edit `run.sh` to set properties `clientId`, and `clientSecret`.)


2) Use the provided shell scripts to test access. 

```bash
$ ./anonymous.sh

$ accessToken=$(./token.sh access_token)
$ ./authenticated.sh $accessToken
$ ./authorised.sh $accessToken

$ idToken=$(./token.sh id_token)
$ ./authenticated.sh $idToken
$ ./authorised.sh $idToken
```

(Edit `./token.sh` to set values for variables `CLIENT_ID`, `CLIENT_SECRET`, `USERNAME`, and `PASSWORD`.)
