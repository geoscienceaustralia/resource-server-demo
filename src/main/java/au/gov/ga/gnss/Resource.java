package au.gov.ga.gnss;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Resource {

    @RequestMapping("/api/anonymous")
    public void anonymous() {
        System.out.println("ok");
    }

    @RequestMapping("/api/authenticated")
    public void authenticated() {
        System.out.println("ok");
    }

    @RequestMapping("/api/authorised")
    public void authorised() {
        System.out.println("ok");
    }
}

