package au.gov.ga.gnss;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManagerResolver;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.DefaultOAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.MappedJwtClaimSetConverter;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.security.oauth2.server.resource.authentication.OpaqueTokenAuthenticationProvider;
import org.springframework.security.oauth2.server.resource.introspection.NimbusOpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionClaimNames;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.web.BearerTokenResolver;
import org.springframework.security.oauth2.server.resource.web.DefaultBearerTokenResolver;

/**
 * Configure OAuth2 resource server that can use both access tokens and id tokens.
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${jwksUri}")
    private String jwksUri;

    @Value("${oauth2Uri}")
    private String oauth2Uri;

    @Value("${clientId}")
    private String clientId;

    @Value("${clientSecret}")
    private String clientSecret;

    @Autowired
    AuthenticationManagerResolver<HttpServletRequest> tokenAuthenticationManagerResolver;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests(authoriseRequests -> authoriseRequests
                .antMatchers(HttpMethod.GET, "/api/anonymous").permitAll()
                .antMatchers(HttpMethod.GET, "/api/authenticated").authenticated()
                .antMatchers(HttpMethod.GET, "/api/authorised").hasAuthority("superuser")
                .anyRequest().denyAll()
            )
            .oauth2ResourceServer()
                .authenticationManagerResolver(this.tokenAuthenticationManagerResolver)
        ;
    }

    @Bean
    public AuthenticationManagerResolver<HttpServletRequest> tokenAuthenticationManagerResolver(
        JwtDecoder jwtDecoder,
        JwtAuthenticationProvider jwtAuthProvider,
        OpaqueTokenAuthenticationProvider opaqueTokenAuthProvider
    ) {
        BearerTokenResolver bearerToken = new DefaultBearerTokenResolver();

        return request -> {
            String token = bearerToken.resolve(request);
            String tokenName = jwtDecoder.decode(token).getClaimAsString("tokenName");

            if ("id_token".equals(tokenName)) {
                return jwtAuthProvider::authenticate;
            }
            if ("access_token".equals(tokenName)) {
                return opaqueTokenAuthProvider::authenticate;
            }
            throw new IllegalStateException("Unrecognised token name: " + String.valueOf(tokenName));
        };
    }

    @Bean
    public JwtAuthenticationProvider jwtAuthProvider(JwtDecoder jwtDecoder) {

        JwtAuthenticationProvider provider = new JwtAuthenticationProvider(jwtDecoder);

        // Convert claim "authorities" to spring-security granted authorities.
        provider.setJwtAuthenticationConverter(new JwtAuthenticationConverter() {
            @Override
            public Collection<GrantedAuthority> extractAuthorities(Jwt jwt) {
                return SecurityConfig.this.grantedAuthorities(jwt.getClaimAsStringList("authorities"));
            }
        });

        return provider;
    }

    @Bean
    public JwtDecoder jwtDecoder() {

        // Validate JWT id token
        NimbusJwtDecoder decoder = NimbusJwtDecoder
            .withJwkSetUri(this.jwksUri)
            .build();

        // Convert claim "sub" to "user_name".
        decoder.setClaimSetConverter(new Converter<Map<String, Object>, Map<String, Object>>() {

            private MappedJwtClaimSetConverter delegate =
                MappedJwtClaimSetConverter.withDefaults(Collections.emptyMap());

            @Override
            public Map<String, Object> convert(Map<String, Object> claims) {
                Map<String, Object> convertedClaims = this.delegate.convert(claims);

                String username = (String) convertedClaims.get("sub");
                convertedClaims.put("user_name", username);

                return convertedClaims;
            }
        });

        return decoder;
    }

    @Bean
    public OpaqueTokenAuthenticationProvider opaqueTokenAuthProvider() {
        ClientRegistration openam = ClientRegistration.withRegistrationId("openam")
            .clientId(this.clientId)
            .clientSecret(this.clientSecret)
            .clientAuthenticationMethod(ClientAuthenticationMethod.BASIC)
            .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
            .scope("openid", "profile")
            .tokenUri(this.oauth2Uri + "/access_token")
            .userInfoUri(this.oauth2Uri + "/userinfo")
            .userNameAttributeName(IdTokenClaimNames.SUB)
            .jwkSetUri(this.jwksUri)
            .build();

        OpaqueTokenIntrospector tokenIntrospector = new OpaqueTokenIntrospector() {

            private OpaqueTokenIntrospector delegate = new NimbusOpaqueTokenIntrospector(
                SecurityConfig.this.oauth2Uri + "/introspect",
                SecurityConfig.this.clientId,
                SecurityConfig.this.clientSecret
            );

            private OAuth2UserService<OAuth2UserRequest, OAuth2User> oauth2UserService = new DefaultOAuth2UserService();

            @Override
            public OAuth2AuthenticatedPrincipal introspect(String token) {
                OAuth2AuthenticatedPrincipal authorized = this.delegate.introspect(token);
                Instant issuedAt = authorized.getAttribute(OAuth2IntrospectionClaimNames.ISSUED_AT);
                Instant expiresAt = authorized.getAttribute(OAuth2IntrospectionClaimNames.EXPIRES_AT);
                OAuth2AccessToken accessToken = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER, token, issuedAt, expiresAt);
                OAuth2UserRequest oauth2UserRequest = new OAuth2UserRequest(openam, accessToken);
                OAuth2AuthenticatedPrincipal principal = this.oauth2UserService.loadUser(oauth2UserRequest);

                @SuppressWarnings("unchecked")
                List<String> authorities = (List<String>) principal.getAttribute("authorities");

                return new DefaultOAuth2AuthenticatedPrincipal(
                    principal.getName(),
                    principal.getAttributes(),
                    SecurityConfig.this.grantedAuthorities(authorities)
                );
            }
        };

        return new OpaqueTokenAuthenticationProvider(tokenIntrospector);
    }

    private Collection<GrantedAuthority> grantedAuthorities(List<String> authoritiesClaim) {
        return authoritiesClaim
            .stream()
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());
    }
}
