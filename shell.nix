{
  pkgs ? import ./nix {},
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    maven
    openjdk8
  ];
}
